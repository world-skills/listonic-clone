package com.example.listonic.api

import com.example.listonic.model.Item
import com.example.listonic.model.ItemGroup
import com.example.listonic.model.User
import com.google.gson.Gson
import okhttp3.*
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Query

const val apiAddress = "http://192.168.1.8:7777"

fun GenerateRequestBody(jsonElement: Any): RequestBody {
    val json = Gson().toJson(jsonElement)

    return RequestBody.create(MediaType.parse("application/json; charset=utf-8"), json)
}

fun GenerateRequestWithUrl(url: String): Request.Builder{
    return Request.Builder().url(url)
}


object ServiceBuilder {
    private val client = OkHttpClient.Builder().build()
    private val retrofit = Retrofit.Builder()
        .baseUrl(apiAddress)
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build()

    fun<T> buildService(service: Class<T>): T{
        return retrofit.create(service)
    }
}

interface BackendEndpoint{
    @PUT("/login")
    fun login(@Body user: User): Call<User>

    @PUT("/register")
    fun register(@Body user: User): Call<User>

    @GET("/get-all-lists")
    fun getAllListGroups(@Query("id") id: Int): Call<ArrayList<ItemGroup>>

    @GET("/get-trash-lists")
    fun getTrashLists(@Query("id") id: Int): Call<ArrayList<ItemGroup>>

    @PUT("/add-new-list")
    fun addNewList(@Body list: ItemGroup): Call<ItemGroup>;

    @PUT("/restore-list")
    fun restoreList(@Query("listID") id: Int): Call<ResponseBody>

    @PUT("update-list")
    fun updateList(@Body list: ItemGroup): Call<ItemGroup>

    @PUT("/move-to-trash")
    fun moveListToTrash(@Body list: ItemGroup): Call<ResponseBody>

    @PUT("delete-list")
    fun deleteList(@Query("listID") id: Int): Call<ResponseBody>

}


