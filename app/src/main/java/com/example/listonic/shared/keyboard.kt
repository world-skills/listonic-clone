package com.example.listonic.shared

import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import kotlinx.android.synthetic.main.activity_item_picker.*

fun openSoftKeyboardOnEditText(window: Window, editText: EditText) {
        if (editText.requestFocus()) {
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        }
}