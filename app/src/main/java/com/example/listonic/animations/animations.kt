package com.example.listonic.animations

import android.animation.ObjectAnimator
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.core.animation.doOnEnd
import androidx.core.animation.doOnStart

fun SlideStatusAnimation(statusTxt: View, actionOnEnd: (() -> Unit) = {}){
    val anim = ObjectAnimator.ofFloat(statusTxt,"translationX", 150F, 0F)
    anim.doOnStart {
        statusTxt.visibility = View.VISIBLE
    }
    anim.duration = 200
    anim.doOnEnd {
        val anim2 = ObjectAnimator.ofFloat(statusTxt, "translationX", 0F, 150F)
        anim2.startDelay = 500
        anim2.duration = 200
        anim2.doOnEnd {
            statusTxt.visibility = View.GONE
            actionOnEnd()
        }
        anim2.start()
    }
    anim.start()
}

fun ScaleAndRotateAnimation(view: View){
    val animDownScaleX = ObjectAnimator.ofFloat(view, "scaleX", 0.5f)
    animDownScaleX.duration = 200
    animDownScaleX.doOnStart {
        val animDownScaleY = ObjectAnimator.ofFloat(view, "scaleY", 0.5f)
        animDownScaleY.duration = 200
        animDownScaleY.doOnEnd {
            val animUpScaleX = ObjectAnimator.ofFloat(view, "scaleX", 1F)
            animUpScaleX.duration = 200
            animUpScaleX.start()
            val animUpScaleY = ObjectAnimator.ofFloat(view, "scaleY", 1F)
            animUpScaleY.duration = 200
            animUpScaleY.start()
        }
        animDownScaleY.start()
        val deg = view.rotation + 180F
        val animRotate = ObjectAnimator.ofFloat(view, "rotation", deg)
        animRotate.duration = 250
        animRotate.interpolator = AccelerateDecelerateInterpolator()
        animRotate.start()
    }
    animDownScaleX.start()
}