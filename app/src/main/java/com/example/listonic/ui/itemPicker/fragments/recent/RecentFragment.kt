package com.example.listonic.ui.itemPicker.fragments.recent

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AutoCompleteTextView
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.listonic.R
import com.example.listonic.datamanager.DataManager
import com.example.listonic.extras.GROUP_ID
import com.example.listonic.ui.itemPicker.fragments.shared.ItemsRecyclerAdapter

class RecentFragment : Fragment() {

    var groupID = -1

    var itemList: RecyclerView? = null

    override fun onResume() {
        super.onResume()
        itemList?.adapter =
            ItemsRecyclerAdapter(
                requireContext(),
                DataManager.filterRecentItems(),
                groupID
            )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_recent, container, false)

        groupID = activity?.intent?.getIntExtra(GROUP_ID, -1) ?: savedInstanceState?.getInt(GROUP_ID) ?: -1

        println("GROUP ID create view $groupID")

        itemList = root.findViewById(R.id.recent_item_list)
        itemList?.layoutManager = LinearLayoutManager(requireContext())
        val searchView = activity?.findViewById<AutoCompleteTextView>(R.id.autocomplete)
        itemList?.adapter =
            ItemsRecyclerAdapter(
                requireContext(),
                DataManager.filterRecentItems(),
                groupID
            )
        searchView?.doOnTextChanged { text, start, count, after ->
            itemList?.adapter =
                ItemsRecyclerAdapter(
                    requireContext(),
                    DataManager.filterRecentItems(text.toString()),
                    groupID
                )
        }
        // Inflate the layout for this fragment
        return root
    }


    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putInt(GROUP_ID, groupID)
    }
}
