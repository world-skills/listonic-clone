package com.example.listonic.ui.my_lists

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.listonic.ui.list_editor.ListEditActivity
import com.example.listonic.R
import com.example.listonic.datamanager.DataManager
import com.example.listonic.extras.GROUP_ID
import com.example.listonic.model.ItemGroup
import com.example.listonic.shared.openSoftKeyboardOnEditText
import com.google.android.material.snackbar.Snackbar
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.time.LocalDateTime

class MyListsRecyclerAdapter(
    val ctx: Context,
    private var itemList: ArrayList<ItemGroup>,
    val window: Window
) :
    RecyclerView.Adapter<MyListsRecyclerAdapter.ViewHolder>() {

    private val inflater = LayoutInflater.from(ctx)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewItem = inflater.inflate(R.layout.mylists_item, parent, false)

        return ViewHolder(viewItem)
    }

    override fun getItemCount(): Int = itemList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val group = itemList[position]
        holder.itemPos = position
        holder.groupName.setText(group.Name)
        holder.groupID = group.ID!!
        holder.itemCounter.text = "${group.itemsChecked}/${group.size}"
        val size = group.itemsCheckedPercentage
        holder.progressBar.progress = size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var groupID = -1
        var itemPos = -1
        val menuBtn = itemView.findViewById<ImageButton>(R.id.menu_button)
        val groupName = itemView.findViewById<EditText>(R.id.group_name)
        val itemCounter = itemView.findViewById<TextView>(R.id.itemCounter)
        val progressBar = itemView.findViewById<ProgressBar>(R.id.progressBar)

        private fun removeView(){

        }

        private fun moveToTrash(){
            val group = DataManager.findGroupByIDMain(groupID)
            try {
                Snackbar.make(itemView, "List moved to trash", Snackbar.LENGTH_LONG)
                val call = DataManager.service.moveListToTrash(group)
                call.enqueue(object: Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        println(t.message)
                    }

                    override fun onResponse(
                        call: Call<ResponseBody>,
                        response: Response<ResponseBody>
                    ) {
                        println(response.body()?.string())
                        if (response.isSuccessful){
                            val index = itemList.indexOfFirst { it.ID == groupID }
                            DataManager.ItemGroups = ArrayList(DataManager.ItemGroups.filter { it.ID != groupID })
                            group.inTrash = true
                            group.Deleted_since = LocalDateTime.now()
                            DataManager.TrashItemGroups.add(group)
                            itemList = DataManager.ItemGroups
                            println("deleted")
                            notifyItemRemoved(index)
                        }
                    }
                })
            }catch (e: Exception){
                Snackbar.make(itemView, "Unable to move to trash", Snackbar.LENGTH_SHORT)
            }
        }

        private fun initateEditGroupName(){
            groupName.isEnabled = true
            openSoftKeyboardOnEditText(window, groupName)
            groupName.setOnEditorActionListener { v, actionId, event ->
                if (actionId == EditorInfo.IME_ACTION_DONE){
                    val list = DataManager.getItemGroupByID(groupID)!!
                    list.Name = v.text.toString()
                    DataManager.service.
                    updateList(list).
                    enqueue(object : Callback<ItemGroup>{
                        override fun onFailure(call: Call<ItemGroup>, t: Throwable) {
                            Snackbar.make(groupName, "Unable to rename group", Snackbar.LENGTH_SHORT)
                        }

                        override fun onResponse(
                            call: Call<ItemGroup>,
                            response: Response<ItemGroup>
                        ) {
                            if (response.isSuccessful){
                                groupName.isEnabled = false
                            }else{
                                Snackbar.make(groupName, "Unable to rename group", Snackbar.LENGTH_SHORT)
                            }
                        }

                    })
                }
                true
            }
        }

        private fun initiateCopyListDialog(){
            val dialog = Dialog(ctx)
            dialog.setContentView(R.layout.copy_list_dialog)
            dialog.window?.attributes?.height = 1000
            dialog.window?.attributes?.width = WindowManager.LayoutParams.MATCH_PARENT
            dialog.window?.attributes?.gravity = Gravity.BOTTOM
            dialog.window?.setWindowAnimations(R.style.Animation_Design_BottomSheetDialog)

            dialog.setOnDismissListener {
                notifyDataSetChanged()
            }

            val purchasedOnlyRb = dialog.findViewById<RadioButton>(R.id.purchasedOnly_rb)
            val enitreListRb = dialog.findViewById<RadioButton>(R.id.entireList_rb)


            purchasedOnlyRb.setOnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked)
                    enitreListRb.isChecked = false
            }

            enitreListRb.setOnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked)
                    purchasedOnlyRb.isChecked = false
            }



            val item_groups =dialog.findViewById<RecyclerView>(R.id.item_group_list_rw)
            item_groups.layoutManager = LinearLayoutManager(ctx)
            item_groups.adapter = CopyDialogRecyclerAdapter(ctx, DataManager.ItemGroups, groupID, dialog)

            val toNewListTxt = dialog.findViewById<TextView>(R.id.toNewList_txt)
            toNewListTxt.setOnClickListener {
                DataManager.copyGroupToNewGroup(groupID, enitreListRb.isChecked){
                    itemList = DataManager.ItemGroups
                    notifyItemInserted(itemList.size -1)
                }

            }

            dialog.show()
        }

        init {
            menuBtn.setOnClickListener {
                val popupMenu = PopupMenu(ctx, menuBtn)
                popupMenu.menuInflater.inflate(R.menu.list_menu, popupMenu.menu)
                popupMenu.setOnMenuItemClickListener { item ->
                    when (item!!.itemId) {
                        R.id.action_rename -> initateEditGroupName()
                        R.id.action_delete -> moveToTrash()
                        R.id.action_share -> println("Share")
                        R.id.action_copy -> initiateCopyListDialog()
                    }
                    true
                }

                popupMenu.show()
            }

            itemView.setOnClickListener {
                val intent = Intent(ctx, ListEditActivity::class.java)
                intent.putExtra(GROUP_ID, groupID)
                ctx.startActivity(intent)
            }
        }
    }
}