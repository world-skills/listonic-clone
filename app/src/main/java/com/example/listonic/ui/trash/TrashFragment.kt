package com.example.listonic.ui.trash

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.listonic.R
import com.example.listonic.api.BackendEndpoint
import com.example.listonic.api.ServiceBuilder
import com.example.listonic.datamanager.DataManager
import com.example.listonic.model.ItemGroup
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [TrashFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TrashFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_trash, container, false)
        val trashList: RecyclerView = root.findViewById(R.id.trash_list)

        val request = ServiceBuilder.buildService(BackendEndpoint::class.java)
        val call = request.getTrashLists(DataManager.user?.ID!!)
        call.enqueue(object: Callback<ArrayList<ItemGroup>>{
            override fun onFailure(call: Call<ArrayList<ItemGroup>>, t: Throwable) {
                Toast.makeText(requireContext(), "${t.message}", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(
                call: Call<ArrayList<ItemGroup>>,
                response: Response<ArrayList<ItemGroup>>
            ) {
                if (response.isSuccessful){
                    trashList.apply {
                        DataManager.TrashItemGroups = response.body()!!
                        layoutManager = LinearLayoutManager(requireContext())
                        adapter = TrashRecyclerAdapter(requireContext(), DataManager.TrashItemGroups)
                    }
                }
            }

        })

        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        updateBackgroundStatus()
    }

    fun showBackgroundImage(){
        val emptyTrashLayout: ConstraintLayout = view?.findViewById(R.id.trash_empty_layout)!!
        emptyTrashLayout.visibility = View.VISIBLE
    }

    fun hideBackgroundImage(){
        val emptyTrashLayout: ConstraintLayout = view?.findViewById(R.id.trash_empty_layout)!!
        emptyTrashLayout.visibility = View.INVISIBLE
    }

    fun updateBackgroundStatus(){
        if (DataManager.TrashItemGroups.size == 0){
            showBackgroundImage()
        }else{
            hideBackgroundImage()
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment TrashFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            TrashFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}
