package com.example.listonic.ui.list_editor

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.listonic.R
import com.example.listonic.extras.GROUP_ID
import com.example.listonic.extras.ITEM_ID
import com.example.listonic.model.ItemGroup

class ListEditRecyclerAdapter(val ctx: Context, var group: ItemGroup) :
    RecyclerView.Adapter<ListEditRecyclerAdapter.ViewHolder>() {

    var priceVisbility = false

    val tempGroup = group.copy(AppListItems = ArrayList(group.AppListItems.filter { it.Amount > 0 }))

    private val layoutInflater = LayoutInflater.from(ctx)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = layoutInflater.inflate(R.layout.inlist_item, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int = tempGroup.AppListItems.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = tempGroup.AppListItems[position]
        holder.cBox.text = item.Name
        holder.cBox.isChecked = item.isChecked
        holder.itemID = item.ID
        holder.itemPos = position
        holder.txtAmount.text = item.Amount.toString()
        holder.icon.setImageDrawable(AppCompatResources.getDrawable(ctx, item.Icon ?: R.drawable.ic_bread))

        holder.priceView.text = "$${item.price}"

        if (priceVisbility){
            holder.icon.visibility = View.INVISIBLE
            holder.txtAmount.visibility = View.INVISIBLE
            holder.priceView.visibility = View.VISIBLE
        }
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var itemID = -1
        var itemPos = -1
        val card = itemView.findViewById<CardView>(R.id.card_item)
        val cBox = itemView.findViewById<CheckBox>(R.id.cBox_item)
        val icon = itemView.findViewById<ImageView>(R.id.icon_item)
        val txtAmount = itemView.findViewById<TextView>(R.id.txtItemAmount)
        val priceView = itemView.findViewById<TextView>(R.id.price_view)

        private fun moveViewToTop(index: Int) {
            card.setBackgroundColor(ctx.resources.getColor(R.color.design_default_color_on_primary))
            notifyItemMoved(index, tempGroup.AppListItems.size - 1)
        }

        private fun moveViewTopBottom(index: Int) {
            notifyItemMoved(index, 0)
        }

        init {
            cBox.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    //Update state and move to end of the list
                    val index = tempGroup.AppListItems.indexOfFirst { it.ID == itemID }
                    if (index != -1) {
                        tempGroup.AppListItems[index].isChecked = isChecked
                        tempGroup.AppListItems.add(tempGroup.AppListItems.removeAt(index))
                        moveViewToTop(index)
                    }
                } else {
                    //Update state and move on top of the list
                    val index = tempGroup.AppListItems.indexOfFirst { it.ID == itemID }
                    if (index != -1) {
                        val item = tempGroup.AppListItems[index]
                        tempGroup.AppListItems[index].isChecked = isChecked
                        tempGroup.AppListItems.removeAt(index)
                        tempGroup.AppListItems.add(0, item)
                        moveViewTopBottom(index)
                    }
                }
            }

            txtAmount.setOnClickListener{
                val intent = Intent(ctx, ItemInspectActivity::class.java)
                intent.putExtra(GROUP_ID, tempGroup.ID)
                intent.putExtra(ITEM_ID, itemID)
                ctx.startActivity(intent)
            }
        }
    }
}