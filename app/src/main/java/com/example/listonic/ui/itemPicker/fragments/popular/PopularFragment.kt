package com.example.listonic.ui.itemPicker.fragments.popular

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AutoCompleteTextView
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.listonic.R
import com.example.listonic.datamanager.DataManager
import com.example.listonic.datamanager.DataManager.filterPopularItems
import com.example.listonic.extras.GROUP_ID
import com.example.listonic.ui.itemPicker.fragments.shared.ItemsRecyclerAdapter

class PopularFragment : Fragment() {
    var groupID = -1
    var itemList: RecyclerView? = null

    override fun onResume() {
        super.onResume()
        itemList?.adapter =
            ItemsRecyclerAdapter(
                requireContext(),
                filterPopularItems(),
                groupID
            )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_popular, container, false)

        itemList = root.findViewById(R.id.popular_recyclerview)

        val searchView = activity?.findViewById<AutoCompleteTextView>(R.id.autocomplete)
        groupID = activity?.intent?.getIntExtra(GROUP_ID, -1) ?: savedInstanceState?.getInt(GROUP_ID) ?: -1


        itemList?.layoutManager = LinearLayoutManager(requireContext())
        itemList?.adapter =
            ItemsRecyclerAdapter(
                requireContext(),
                filterPopularItems(),
                groupID
            )
        searchView?.doOnTextChanged { text, start, count, after ->
            itemList?.adapter =
                ItemsRecyclerAdapter(
                    requireContext(),
                    filterPopularItems(text.toString()),
                    groupID
                )
        }

        return root
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(GROUP_ID, groupID)
    }
}
