package com.example.listonic.ui.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.listonic.MainActivity
import com.example.listonic.R
import com.example.listonic.api.*
import com.example.listonic.datamanager.DataManager
import com.example.listonic.model.User
import com.example.listonic.ui.register.RegisterActivity
import com.google.gson.GsonBuilder
import okhttp3.Response
import retrofit2.Call
import retrofit2.Callback
import java.io.IOException
import java.lang.Exception

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val emailTB = findViewById<EditText>(R.id.email_tb)
        val passwordTB = findViewById<EditText>(R.id.password_tb)


        val loginBtn = findViewById<Button>(R.id.login_btn)


        loginBtn.setOnClickListener{
            val user = User(Email = emailTB.text.toString(), Password = passwordTB.text.toString())
            loginUser(user)
        }

        val registerNav = findViewById<Button>(R.id.register_btn)
        registerNav.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }


    }


    fun loginUser(user: User){
        val request = ServiceBuilder.buildService(BackendEndpoint::class.java)
        val call = request.login(user)

        call.enqueue(object: Callback<User>{
            override fun onFailure(call: Call<User>, t: Throwable) {
                Toast.makeText(this@LoginActivity, "${t.message}", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<User>, response: retrofit2.Response<User>) {
                if (response.isSuccessful){
                    DataManager.user = response.body()

                    val intent = Intent(this@LoginActivity, MainActivity::class.java)
                    startActivity(intent)
                }
            }
        })
    }
}
