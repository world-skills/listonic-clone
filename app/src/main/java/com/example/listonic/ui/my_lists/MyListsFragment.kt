package com.example.listonic.ui.my_lists

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.listonic.AddListActivity
import com.example.listonic.R
import com.example.listonic.api.BackendEndpoint
import com.example.listonic.api.ServiceBuilder
import com.example.listonic.datamanager.DataManager
import com.example.listonic.extras.GROUP_ID
import com.example.listonic.model.ItemGroup
import com.google.android.material.floatingactionbutton.FloatingActionButton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyListsFragment : Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_my_lists, container, false)

        val itemList = root.findViewById<RecyclerView>(R.id.main_item_list)

        val request = ServiceBuilder.buildService(BackendEndpoint::class.java)
        val call = request.getAllListGroups(DataManager.user?.ID!!)

        call.enqueue(object: Callback<ArrayList<ItemGroup>>{
            override fun onFailure(call: Call<ArrayList<ItemGroup>>, t: Throwable) {
                Toast.makeText(this@MyListsFragment.requireContext(), "${t.message}", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(
                call: Call<ArrayList<ItemGroup>>,
                response: Response<ArrayList<ItemGroup>>) {
                if (response.isSuccessful){
                    itemList.apply {
                        itemList.layoutManager = LinearLayoutManager(requireContext())
                        DataManager.ItemGroups = response.body()!!
                        itemList?.adapter = MyListsRecyclerAdapter(requireContext(), DataManager.ItemGroups, activity?.window!!)
                        updateBackgroundStatus()
                    }
                }
            }

        })


        val addListBtn = root.findViewById<FloatingActionButton>(R.id.floatingAddList)
        addListBtn.setOnClickListener {
            val intent = Intent(context, AddListActivity::class.java)
            context?.startActivity(intent)
        }

        return root
    }
    fun showBackgroundImage(){
        val emptyLayout = view?.findViewById<ConstraintLayout>(R.id.my_list_empty_layout)
        emptyLayout?.visibility = View.VISIBLE
    }

    fun hideBackgroundImage(){
        val emptyLayout = view?.findViewById<ConstraintLayout>(R.id.my_list_empty_layout)
        emptyLayout?.visibility = View.INVISIBLE
    }

    fun updateBackgroundStatus(){
        if (DataManager.ItemGroups.size == 0){
            showBackgroundImage()
        }else{
            hideBackgroundImage()
        }
    }

    override fun onResume() {
        super.onResume()
        view?.findViewById<RecyclerView>(R.id.main_item_list)?.adapter?.notifyDataSetChanged()
        updateBackgroundStatus()
    }
}
