package com.example.listonic.ui.list_editor

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.listonic.R
import com.example.listonic.api.BackendEndpoint
import com.example.listonic.api.ServiceBuilder
import com.example.listonic.datamanager.DataManager
import com.example.listonic.extras.GROUP_ID
import com.example.listonic.model.Item
import com.example.listonic.model.ItemGroup
import com.example.listonic.ui.itemPicker.ItemPicker
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ListEditActivity : AppCompatActivity() {

    private var groupID = -1

    private var itemList: RecyclerView? = null
    private var group: ItemGroup? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_edit)
        supportActionBar?.hide()

        groupID = intent.getIntExtra(GROUP_ID, -1)

        group =DataManager.findGroupByIDMain(groupID)
        val title = findViewById<TextView>(R.id.list_name_title)
        title.text = group?.Name
        val returnBtn = findViewById<ImageButton>(R.id.btn_return)
        returnBtn.setOnClickListener {
            finish()
        }


        itemList = findViewById<RecyclerView>(R.id.items_list)
        itemList?.layoutManager = LinearLayoutManager(this)
        val adapter = ListEditRecyclerAdapter(this, group!!)
        itemList?.adapter = adapter

        val optionsBtn = findViewById<ImageButton>(R.id.btn_options)
        optionsBtn.setOnClickListener {
            val popup = PopupMenu(this, optionsBtn)
            popup.menuInflater.inflate(R.menu.list_edit_menu, popup.menu)
            if (group?.itemsChecked == 0){
                popup.menu.findItem(R.id.delete_pur_items_action).isEnabled = false
            }

            if (adapter.priceVisbility){
                popup.menu.getItem(1).title = "Hide prices"
            }else{
                popup.menu.getItem(1).title = "Show prices"
            }

            popup.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.show_price_action -> {
                        adapter.priceVisbility = !adapter.priceVisbility
                        itemList?.adapter = adapter
                        if (adapter.priceVisbility){
                            popup.menu.getItem(1).title = "Hide prices"
                        }else{
                            popup.menu.getItem(1).title = "Show prices"
                        }
                    }
                    else -> {
                        group?.AppListItems?.forEach {
                            if(it.isChecked)
                                it.Amount = 0
                        }

                        DataManager.service.updateList(group!!).enqueue(object: Callback<ItemGroup>{
                            override fun onFailure(call: Call<ItemGroup>, t: Throwable) {
                                Toast.makeText(this@ListEditActivity, "error occure ${t.message}", Toast.LENGTH_SHORT).show()
                            }

                            override fun onResponse(
                                call: Call<ItemGroup>,
                                response: Response<ItemGroup>
                            ) {
                                if(response.isSuccessful){
                                    itemList?.adapter = ListEditRecyclerAdapter(this@ListEditActivity, response.body()!!)
                                }
                            }

                        })
                    }
                }
                true
            }
            popup.show()
        }


        val pickerBtn = findViewById<FloatingActionButton>(R.id.floatBtn_startItemPicker)
        pickerBtn.setOnClickListener {
            val intent = Intent(this, ItemPicker::class.java)
            intent.putExtra(GROUP_ID, groupID)
            this.startActivity(intent)
        }
    }

    override fun onDestroy() {
        val request = ServiceBuilder.buildService(BackendEndpoint::class.java)

        var list = DataManager.findGroupByIDMain(groupID)
        val call = request.updateList(list)

        call.enqueue(object: Callback<ItemGroup> {
            override fun onFailure(call: Call<ItemGroup>, t: Throwable) {
                Snackbar.make(itemList!!,"${t.message}", Snackbar.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<ItemGroup>, response: Response<ItemGroup>) {
                if (response.isSuccessful){
                    list = response.body()!!
                }
            }


        })


        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()

        if (groupID != -1) {
            itemList?.layoutManager = LinearLayoutManager(this)
            itemList?.adapter = ListEditRecyclerAdapter(this, group!!)
        }
    }

    override fun onStart() {
        super.onStart()
        itemList?.adapter?.notifyDataSetChanged()
    }
}
