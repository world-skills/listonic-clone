package com.example.listonic.ui.itemPicker.fragments.itemcatalog

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.listonic.R
import com.example.listonic.datamanager.DataManager
import com.example.listonic.datamanager.DataManager.itemsByCatID
import com.example.listonic.ui.itemPicker.fragments.shared.ItemsRecyclerAdapter

class ItemCategoryFragment(val categoryID: Int,val groupID: Int) : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root =  inflater.inflate(R.layout.fragment_item_category, container, false)

        val itemList = root.findViewById<RecyclerView>(R.id.item_list)

        itemList.layoutManager = LinearLayoutManager(requireContext())
        itemList.adapter = ItemsRecyclerAdapter(requireContext(), itemsByCatID(categoryID), groupID)
        val itemlistAnimation = AnimationUtils.loadAnimation(requireContext(), R.anim.slide_from_bottom)
        itemList.animation = itemlistAnimation


        val nextCatImage = root.findViewById<ImageView>(R.id.nextCat_image)
        val nextCatText = root.findViewById<TextView>(R.id.nextCat_name_txt)
        var nextCat = DataManager.getNextCategory(categoryID)

        nextCatImage.setImageDrawable(AppCompatResources.getDrawable(requireContext(), nextCat.icon))
        nextCatText.text = nextCat.Name


        val nextCatOverlay = root.findViewById<ConstraintLayout>(R.id.nextCat_overlay)

        nextCatOverlay.setOnClickListener{
            nextCat = DataManager.getNextCategory(nextCat.ID)
            itemList.adapter = ItemsRecyclerAdapter(requireContext(), itemsByCatID(nextCat.ID), groupID)
            nextCatImage.setImageDrawable(AppCompatResources.getDrawable(requireContext(), nextCat.icon))
            nextCatText.text = nextCat.Name
            itemList.animation = itemlistAnimation
            itemList.animation.startNow()
        }

        return root
    }
}
