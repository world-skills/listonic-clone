package com.example.listonic.ui.itemPicker.fragments.itemcatalog

import android.content.Context
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.example.listonic.R
import com.example.listonic.datamanager.itemCategories

class CategoryRecyclerAdapter(val context: Context,val  manager : FragmentManager,val groupID: Int) : RecyclerView.Adapter<CategoryRecyclerAdapter.ViewHolder>() {

    private val inflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = inflater.inflate(R.layout.catalog_item, parent, false)

        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int = itemCategories.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val cat = itemCategories[position]

        holder.txtName.text = cat.Name
        holder.image.setImageDrawable(AppCompatResources.getDrawable(context, cat.icon))
        holder.catID = cat.ID
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val txtName = itemView.findViewById<TextView>(R.id.txt_category_name)
        val image = itemView.findViewById<ImageView>(R.id.category_image)
        var catID = -1

        init {
            itemView.setOnClickListener {
                manager.beginTransaction().replace(R.id.catalog_fragment_container, ItemCategoryFragment(catID, groupID)).addToBackStack(null).commit()
            }
        }
    }

}