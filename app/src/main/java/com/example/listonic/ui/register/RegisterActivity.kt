package com.example.listonic.ui.register

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.listonic.MainActivity
import com.example.listonic.R
import com.example.listonic.datamanager.DataManager
import com.example.listonic.model.User
import com.google.android.material.snackbar.Snackbar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        val registerBtn = findViewById<Button>(R.id.register_btn)
        registerBtn.setOnClickListener {
            registerUser()
        }

    }


    fun registerUser(){
        val emailTxt = findViewById<EditText>(R.id.txt_email)
        val passwordTxt = findViewById<EditText>(R.id.txt_password)
        val firstNameTxt = findViewById<EditText>(R.id.txt_firstName)
        val lastNameTxt = findViewById<EditText>(R.id.txt_lastName)

        val email = emailTxt.text
        val password = passwordTxt.text
        val fname = firstNameTxt.text
        val lname = lastNameTxt.text

        var valid = true

        if (email.isNullOrEmpty()){
            Toast.makeText(this, "Invalid email input", Toast.LENGTH_SHORT).show()
            valid = false
        }

        if (password.length < 5){
            Toast.makeText(this, "password should be longer than 5 characters",Toast.LENGTH_SHORT).show()
            valid = false
        }

        if (fname.isNullOrEmpty()){
            Toast.makeText(this, "First name should not be empty",Toast.LENGTH_SHORT).show()
            valid = false
        }

        if (lname.isNullOrEmpty()){
            Toast.makeText(this, "Last name should not be empty",Toast.LENGTH_SHORT).show()
            valid = false
        }

        if (!valid)
            return

        val user = User(Email = email.toString(), Password = password.toString(), FirstName = fname.toString(), LastName = lname.toString())

        DataManager.service.register(user).enqueue(object: Callback<User> {
            override fun onFailure(call: Call<User>, t: Throwable) {
                Toast.makeText(this@RegisterActivity,"Error occured while registering", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<User>, response: Response<User>) {

                if (response.isSuccessful){
                    DataManager.user = response.body()!!

                    val intent = Intent(this@RegisterActivity, MainActivity::class.java)
                    startActivity(intent)
                }

            }

        })


    }


}
