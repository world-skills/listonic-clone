package com.example.listonic.ui.my_lists

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.listonic.R
import com.example.listonic.datamanager.DataManager
import com.example.listonic.model.ItemGroup

class CopyDialogRecyclerAdapter(val context: Context, var groups: ArrayList<ItemGroup>, val groupToCopyID: Int,val dialog: Dialog) : RecyclerView.Adapter<CopyDialogRecyclerAdapter.ViewHolder>() {

    val inflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val viewItem = inflater.inflate(R.layout.copy_list_item, parent, false)

        return ViewHolder(viewItem)
    }

    override fun getItemCount(): Int = groups.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val group = groups[position]
        holder.groupNametxt.text = group.Name
        holder.groupItemCounter.text = "${group.itemsChecked}/${group.AppListItems.size}"
        holder.groupID = group.ID!!
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var groupID = -1
        val groupNametxt = itemView.findViewById<TextView>(R.id.group_name_txt)
        val groupItemCounter = itemView.findViewById<TextView>(R.id.group_item_counter_txt)



        init {
            itemView.setOnClickListener {
                DataManager.copyGroupToGroup(groupToCopyID, groupID, dialog.findViewById<RadioButton>(R.id.entireList_rb).isChecked)
                                                                     dialog.dismiss()
            }
        }
    }

}