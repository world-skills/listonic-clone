package com.example.listonic.ui.itemPicker

import android.Manifest.permission.RECORD_AUDIO
import android.app.Activity
import android.app.Dialog
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.opengl.Visibility
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.view.*
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.listonic.R
import com.example.listonic.api.BackendEndpoint
import com.example.listonic.api.ServiceBuilder
import com.example.listonic.datamanager.DataManager
import com.example.listonic.extras.GROUP_ID
import com.example.listonic.model.ItemGroup
import com.example.listonic.shared.openSoftKeyboardOnEditText
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_item_picker.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.speech_recognition_dialog.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import java.util.jar.Manifest

class ItemPicker : AppCompatActivity() {
    private val RECORD_REQUEST_CODE = 101

    private var mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this)
    private var speechIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)

    var groupID = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_picker)
        setSupportActionBar(toolBar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        val fragmentAdapter = PagerAdapter(supportFragmentManager)
        viewPager.adapter = fragmentAdapter
        viewPager.animation = AnimationUtils.loadAnimation(this, R.anim.slide_from_right)

        groupID = intent.getIntExtra(GROUP_ID, -1)

        openSoftKeyboardOnEditText(window, autocomplete)

        btn_back.setOnClickListener {
            finish()
        }

        btn_speech.setOnClickListener {
            speechIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
            speechIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, Locale.getDefault())
            speechIntent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Hi speak something");

            mSpeechRecognizer.setRecognitionListener(SpeechListenerManager(this))
            val permission = ContextCompat.checkSelfPermission(this, RECORD_AUDIO)
            if (permission != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, arrayOf(RECORD_AUDIO), RECORD_REQUEST_CODE)
            } else {
                mSpeechRecognizer.startListening(speechIntent)
            }
        }

        tabLayout.setupWithViewPager(viewPager)
    }

    inner class SpeechListenerManager(val context: Context) : RecognitionListener {
        val dialog = Dialog(context)

        private fun showDialog(){
            dialog.setContentView(R.layout.speech_recognition_dialog)
            dialog.show()
        }

        override fun onReadyForSpeech(params: Bundle?) {
            showDialog()
        }

        override fun onRmsChanged(rmsdB: Float) {}

        override fun onBufferReceived(buffer: ByteArray?) {
            println("onBufferReceived")
        }

        override fun onPartialResults(partialResults: Bundle?) {}

        override fun onEvent(eventType: Int, params: Bundle?) {}

        override fun onBeginningOfSpeech() {}

        override fun onEndOfSpeech() {
            dialog.hide()
        }

        override fun onError(error: Int) {
            dialog.findViewById<TextView>(R.id.speechText).text = getSpeechErrorMessage(error)
            val btnTryAgain = dialog.findViewById<Button>(R.id.btn_tryAgain)

            btnTryAgain.visibility = View.VISIBLE

            btnTryAgain.setOnClickListener {
                mSpeechRecognizer.startListening(speechIntent)
                btnTryAgain.visibility = View.INVISIBLE
            }
        }

        override fun onResults(results: Bundle?) {
            val matches = results?.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)
            autocomplete.setText(matches?.get(0))
        }

        fun getSpeechErrorMessage(code: Int): String {
            return when (code) {
                SpeechRecognizer.ERROR_AUDIO -> "Audio recording error"
                SpeechRecognizer.ERROR_CLIENT -> "Other client side errors"
                SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS -> "Insufficient permissions"
                SpeechRecognizer.ERROR_NETWORK -> "Network related errors"
                SpeechRecognizer.ERROR_NETWORK_TIMEOUT -> "Network operation timed out"
                SpeechRecognizer.ERROR_NO_MATCH -> "No recognition result matched"
                SpeechRecognizer.ERROR_RECOGNIZER_BUSY -> "RecognitionService busy"
                SpeechRecognizer.ERROR_SERVER -> "Server sends error status"
                SpeechRecognizer.ERROR_SPEECH_TIMEOUT -> "No speech input"
                else -> "Unexpected error"
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RECORD_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            val values = data?.getStringArrayExtra(RecognizerIntent.EXTRA_RESULTS)?.get(0)!!
            autocomplete.setText(values)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == RECORD_REQUEST_CODE) {
            if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "permission denied", Toast.LENGTH_SHORT).show()
            } else {
                mSpeechRecognizer.startListening(speechIntent)
            }
        }
    }

}
