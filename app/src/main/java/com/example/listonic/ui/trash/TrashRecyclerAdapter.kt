package com.example.listonic.ui.trash

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.listonic.R
import com.example.listonic.api.BackendEndpoint
import com.example.listonic.api.ServiceBuilder
import com.example.listonic.datamanager.DataManager
import com.example.listonic.model.ItemGroup
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.format.DateTimeFormatter
import kotlin.reflect.KFunction0

class TrashRecyclerAdapter(
    ctx: Context,
    private var trashItems: ArrayList<ItemGroup>
) :
    RecyclerView.Adapter<TrashRecyclerAdapter.ViewHolder>() {

    private val layoutInflater = LayoutInflater.from(ctx)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = layoutInflater.inflate(R.layout.trash_list_item, parent, false)

        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int = trashItems.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = trashItems[position]
        holder.itemNameTxt.text = item.Name
        holder.itemDeletedSinceTxt.text =
            item.Deleted_since?.format(DateTimeFormatter.ofPattern("M/d/y"))
        holder.groupID = item.ID!!
        holder.itemPos = position
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var groupID = -1
        var itemPos = -1
        val itemNameTxt = itemView.findViewById<TextView>(R.id.group_name)
        val itemDeletedSinceTxt = itemView.findViewById<TextView>(R.id.item_deleted_since)

        val btnDeletePerma = itemView.findViewById<Button>(R.id.btn_del_perma)
        val btnRestore = itemView.findViewById<Button>(R.id.btn_restore)

        init {
            btnDeletePerma.setOnClickListener {
                DataManager.permaDeleteGroup(groupID)
                DataManager.service.deleteList(groupID).enqueue(object: Callback<ResponseBody>{
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        println(t.message)
                    }

                    override fun onResponse(
                        call: Call<ResponseBody>,
                        response: Response<ResponseBody>
                    ) {
                        val index = trashItems.indexOfFirst { it.ID == groupID }
                        DataManager.TrashItemGroups = ArrayList(DataManager.TrashItemGroups.filter { it.ID != groupID })
                        trashItems = DataManager.TrashItemGroups
                        notifyItemRemoved(index)
                    }
                })
            }

            btnRestore.setOnClickListener {
                val call = DataManager.service.restoreList(groupID)
                call.enqueue(object: Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        println(t.message)
                    }

                    override fun onResponse(
                        call: Call<ResponseBody>,
                        response: Response<ResponseBody>
                    ) {
                        val index = trashItems.indexOfFirst { it.ID == groupID }
                        DataManager.TrashItemGroups = ArrayList(DataManager.TrashItemGroups.filter { it.ID != groupID })
                        trashItems = DataManager.TrashItemGroups
                        notifyItemRemoved(index)
                    }

                })
            }
        }
    }
}