package com.example.listonic.ui.list_editor

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import com.example.listonic.MainActivity
import com.example.listonic.R
import com.example.listonic.datamanager.DataManager
import com.example.listonic.extras.GROUP_ID
import com.example.listonic.extras.ITEM_ID
import com.example.listonic.model.Item
import com.example.listonic.model.ItemGroup
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ItemInspectActivity : AppCompatActivity() {
    private var groupID = -1
    private var itemID = -1

    private var itemNameEditTxt: EditText? = null
    private var quantityEditText: EditText? = null
    private var unitEditText: EditText? = null
    private var notesEditText: EditText? = null
    private var priceEditText: EditText? = null
    private var cameraBtn: FloatingActionButton? = null

    var group: ItemGroup? = null
    var listItem: Item? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item_inspect)

        val actionbar = supportActionBar
        groupID = intent.getIntExtra(GROUP_ID, -1)
        itemID = intent.getIntExtra(ITEM_ID, -1)
        actionbar!!.setDisplayShowTitleEnabled(false)
        actionbar.setDisplayHomeAsUpEnabled(true)

        group = DataManager.findGroupByIDMain(groupID)
        listItem = group?.findItemByID(itemID)
        val category = DataManager.getCategoryByID(listItem?.type!!)

        val categoryText = findViewById<TextView>(R.id.txt_category)
        val categoryImage = findViewById<ImageView>(R.id.category_image)
        itemNameEditTxt = findViewById(R.id.itemName_edittxt)
        quantityEditText = findViewById(R.id.quantity_txt)
        unitEditText = findViewById(R.id.unit_txt)
        notesEditText = findViewById(R.id.notes_text)
        priceEditText = findViewById(R.id.price_txt)
        cameraBtn = findViewById(R.id.camera_btn)

        cameraBtn?.setOnClickListener {
            val pickPhotoIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(pickPhotoIntent, 1)
        }


        categoryText.text = category.Name
        itemNameEditTxt?.setText(listItem?.Name)
        categoryImage.setImageDrawable(AppCompatResources.getDrawable(this, category.icon))
        quantityEditText?.setText(listItem?.Amount.toString())
        unitEditText?.setText(listItem?.unit)
        notesEditText?.setText(listItem?.Notes)
        priceEditText?.setText(listItem?.price.toString())

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.item_inspect_menu, menu)

        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val group = DataManager.findGroupByIDMain(groupID)
        val item = group.findItemByID(itemID)

        item.Name = itemNameEditTxt?.text.toString()
        item.Amount = quantityEditText?.text.toString().toInt()
        item.Notes = notesEditText?.text.toString()
        item.unit = unitEditText?.text.toString()
        item.price = priceEditText?.text.toString().toFloat()
        onBackPressed()
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId){
        R.id.action_delete ->{
            listItem?.Amount = 0
            finish()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }
}
