package com.example.listonic.ui.itemPicker.fragments.itemcatalog

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.listonic.R

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class ItemCatalogFragment(val manager: FragmentManager,val groupID: Int) : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_item_catalog, container, false)

        val categoryList = root.findViewById<RecyclerView>(R.id.category_rc_view)

        categoryList.layoutManager = LinearLayoutManager(requireContext())
        categoryList.adapter = CategoryRecyclerAdapter(requireContext(),manager,groupID)

        return root
    }
}
