package com.example.listonic.ui.itemPicker

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.listonic.ui.itemPicker.fragments.itemcatalog.ItemCatalogRoot
import com.example.listonic.ui.itemPicker.fragments.popular.PopularFragment
import com.example.listonic.ui.itemPicker.fragments.recent.RecentFragment


class PagerAdapter(fm : FragmentManager) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): Fragment {
        return when (position){
            0 -> RecentFragment()
            1 -> PopularFragment()
            else -> ItemCatalogRoot()
        }
    }

    override fun getCount(): Int = 3

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "Recent"
            1 -> "Popular"
            else -> "Item Catalog"
        }
    }
}