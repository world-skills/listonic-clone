package com.example.listonic.ui.itemPicker.fragments.itemcatalog

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

import com.example.listonic.R
import com.example.listonic.extras.GROUP_ID

class ItemCatalogRoot : Fragment() {
    var groupID = -1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root = inflater.inflate(R.layout.fragment_item_catalog_root, container, false)
        groupID = activity?.intent?.getIntExtra(GROUP_ID, -1) ?: savedInstanceState?.getInt(
            GROUP_ID
        ) ?: -1
        childFragmentManager.beginTransaction().replace(R.id.catalog_fragment_container, ItemCatalogFragment(childFragmentManager, groupID)).addToBackStack(null).commit()
        println("yeah")
        return root
    }
}
