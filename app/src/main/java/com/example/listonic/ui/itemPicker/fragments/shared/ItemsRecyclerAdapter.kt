package com.example.listonic.ui.itemPicker.fragments.shared

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.listonic.R
import com.example.listonic.animations.ScaleAndRotateAnimation
import com.example.listonic.animations.SlideStatusAnimation
import com.example.listonic.datamanager.DataManager
import com.example.listonic.datamanager.DataManager.getPopularItemByID
import com.example.listonic.model.Item

class ItemsRecyclerAdapter(val context: Context,private val items: ArrayList<Item>, val groupID: Int) :
    RecyclerView.Adapter<ItemsRecyclerAdapter.ViewHolder>() {

    private val inflater = LayoutInflater.from(context)

    private val newList = DataManager.getItemGroupByID(groupID)

    init {
        newList?.AppListItems?.forEach { gitem ->
            items.find{
                it.defaultName == gitem.defaultName
            }?.Amount = gitem.Amount
        }
    }



    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtName = itemView.findViewById<TextView>(R.id.nameTxt)
        val btnInc = itemView.findViewById<ImageButton>(R.id.incrementItemBtn)
        val amountTxt = itemView.findViewById<TextView>(R.id.amountTxt)
        val statusTxt = itemView.findViewById<TextView>(R.id.statusText)
        val deleteAmountBtn = itemView.findViewById<ImageButton>(R.id.btnDeleteAmountAll)
        var itemPos = -1
        var itemID = -1
        var amount = 0

        fun addItem(){
            renderAmount()
            val item = getPopularItemByID(itemID)!!
            DataManager.findGroupByIDMain(groupID).addItem(item)
            DataManager.updateRecentItems(item)
        }

        fun removeItem(){
            renderAmount()
            val item = getPopularItemByID(itemID)!!
            DataManager.findGroupByIDMain(groupID).removeItem(item)
        }

        fun renderAmount(){
            amountTxt.text = amount.toString()
        }

        fun showDeletedStatus(){
            statusTxt.text = "Deleted"
            SlideStatusAnimation(statusTxt)
            statusTxt.setTextColor(context.resources.getColor(R.color.redPrimary))
        }

        fun showDeleteButton(){
            deleteAmountBtn.visibility = View.VISIBLE
        }

        init {
            deleteAmountBtn.setOnClickListener {
                if (--amount == 0){
                    deleteAmountBtn.visibility = View.INVISIBLE
                    amountTxt.visibility = View.INVISIBLE
                }

                removeItem()
                showDeletedStatus()
            }

            btnInc.setOnClickListener {
                btnInc.clearAnimation()
                if (amount++ == 0){
                    amountTxt.visibility = View.VISIBLE
                    statusTxt.text = "Added"
                    statusTxt.setTextColor(context.resources.getColor(R.color.colorPrimary))
                    SlideStatusAnimation(statusTxt) {
                        showDeleteButton()
                    }
                }

                addItem()

                ScaleAndRotateAnimation(btnInc)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = inflater.inflate(R.layout.itempicker_item, parent, false)

        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.txtName.text = item.Name
        holder.itemPos = position
        holder.amountTxt.text = item.Amount.toString()
        holder.amount = item.Amount
        if (item.Amount > 0) {
            holder.showDeleteButton()
            holder.amountTxt.visibility = View.VISIBLE
        }
        holder.itemID = item.ID
    }
}