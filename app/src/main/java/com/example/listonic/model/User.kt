package com.example.listonic.model

data class User(val ID: Int = -1, val Email: String = "", val FirstName: String = "", val LastName: String = "", val Password: String = "")