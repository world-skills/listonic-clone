package com.example.listonic.model

import com.example.listonic.api.BackendEndpoint
import com.example.listonic.api.ServiceBuilder
import java.time.LocalDateTime
import kotlin.collections.ArrayList

data class ItemGroup (val ID:Int = -1, var Name:String, var AppListItems: ArrayList<Item> = ArrayList(), var inTrash: Boolean = false, var Deleted_since: LocalDateTime? = null, val UserID: Int? = null){
    fun addItem(item:Item){
        item.ListID = ID
        val itemMatch = AppListItems.find { it.defaultName == item.defaultName }
        if (itemMatch == null){
            AppListItems.add(item.copy(Amount = 1))
        }else{
            itemMatch.Amount += 1
        }
    }

    val size: Int
    get() = AppListItems.filter { it.Amount > 0 }.size

    fun removeItem(item: Item){
        val itemMatch = AppListItems.find { it.defaultName == item.defaultName }!!
        itemMatch.Amount--
    }

    fun findItemByID(id: Int): Item = AppListItems.firstOrNull { it.ID == id }!!

    val itemsChecked: Int
        get() = AppListItems.filter { it.isChecked && it.Amount > 0 }.size

    val itemsCheckedPercentage: Int
        get() {
            if (size == 0)
                return 0
            else
                return ((AppListItems.filter { it.isChecked }.size.toFloat() / AppListItems.size.toFloat()) * 100).toInt()
        }
}


