package com.example.listonic.model

import com.example.listonic.R
import com.example.listonic.datamanager.popularItems


data class Item(val ID:Int,
                var Name: String,
                var isChecked: Boolean = false,
                var Amount:Int = 1,
                val Icon: Int? = null,
                val type: Int,
                var unit:String = "",
                var Notes:String = "",
                var price: Float = 0f,
                var ListID: Int = -1,
                var defaultName: String = Name){
}

class ItemCategory(val ID: Int, val Name: String, val icon: Int)
