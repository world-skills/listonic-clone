package com.example.listonic.datamanager

import com.example.listonic.R
import com.example.listonic.api.BackendEndpoint
import com.example.listonic.api.ServiceBuilder
import com.example.listonic.model.Item
import com.example.listonic.model.ItemGroup
import com.example.listonic.model.ItemCategory
import com.example.listonic.model.User
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.Exception
import java.time.LocalDateTime

object DataManager {
    var TrashItemGroups = ArrayList<ItemGroup>()
    var ItemGroups = ArrayList<ItemGroup>()
    var RecentItems = ArrayList<Item>()
    var user: User? = null

    val service = ServiceBuilder.buildService(BackendEndpoint::class.java)



    fun FetchGroups(){
        
    }


    init {
        // createDummyGroup()
    }



    fun copyGroupToGroup(fromID: Int, toId: Int, entireList: Boolean,onSuccess: () -> Unit = {}){
        val fromG = getItemGroupByID(fromID)
        val toG = getItemGroupByID(toId)

        if (entireList){
            fromG?.AppListItems?.forEach { fItem ->
                if (toG?.AppListItems?.firstOrNull { tItem -> tItem.defaultName == fItem.defaultName } == null){
                    val item = fItem
                    item.ListID = toG?.ID!!
                    toG.AppListItems.add(item)
                }
            }
        }else{
            fromG?.AppListItems?.forEach { fItem ->
                if (toG?.AppListItems?.firstOrNull { tItem -> tItem.defaultName == fItem.defaultName } == null && !fItem.isChecked){
                    val item = fItem
                    item.ListID = toG?.ID!!
                    toG.AppListItems.add(item)
                }
            }
        }

        service.updateList(toG!!).enqueue(object:Callback<ItemGroup>{
            override fun onFailure(call: Call<ItemGroup>, t: Throwable) {
                println(t.message)
            }

            override fun onResponse(call: Call<ItemGroup>, response: Response<ItemGroup>) {
                if (response.isSuccessful){
                    onSuccess()
                }
            }

        })
    }

    fun copyGroupToNewGroup(id: Int, entireList: Boolean, onSuccess: () -> Unit = {}){
        val group = getItemGroupByID(id)
        val newGroup = group?.copy(Name =  group.Name, UserID = user?.ID)!!
        if (entireList){
            newGroup.AppListItems = ArrayList(group.AppListItems)

            service.addNewList(newGroup).enqueue(object: Callback<ItemGroup>{
                override fun onFailure(call: Call<ItemGroup>, t: Throwable) {
                    println(t.message)
                }

                override fun onResponse(call: Call<ItemGroup>, response: Response<ItemGroup>) {
                    println(response.errorBody()?.string())
                    if (response.isSuccessful){
                        ItemGroups.add(response.body()!!)
                        onSuccess()
                    }
                }
            })
        }else{
            newGroup.AppListItems = ArrayList(group.AppListItems.filter { !it.isChecked })
            service.addNewList(newGroup).enqueue(object: Callback<ItemGroup>{
                override fun onFailure(call: Call<ItemGroup>, t: Throwable) {
                    println(t.message)
                }

                override fun onResponse(call: Call<ItemGroup>, response: Response<ItemGroup>) {
                    if (response.isSuccessful){
                        ItemGroups.add(response.body()!!)
                        onSuccess()
                    }
                }
            })

        }
    }

    fun filterPopularItems(name: String = ""): ArrayList<Item> =
        ArrayList(popularItems.filter { it.Name.toLowerCase().contains(name.toLowerCase()) })

    fun getPopularItemByID(id: Int): Item? = popularItems.find { it.ID == id }

    fun getNextCategory(currentID: Int): ItemCategory {
        val index = itemCategories.indexOfFirst {
            it.ID == currentID
        }
        if (index == itemCategories.size - 1) {
            return itemCategories[0]
        }else{
            return itemCategories[index+1]
        }
    }
        fun createDummyGroup(){
            ItemGroups.add(ItemGroup(0, "Konzum", popularItems))
        }

        fun getCategoryByID(id: Int): ItemCategory = itemCategories.firstOrNull { it.ID == id}!!

        fun itemsByCatID(id: Int): ArrayList<Item> = ArrayList(popularItems.filter { it.type == id })

        fun filterRecentItems(query: String = ""): ArrayList<Item> = ArrayList(RecentItems.filter { it.Name.toLowerCase().contains(query.toLowerCase()) })
        fun addNewItemGroup(list: ItemGroup) {
            ItemGroups.add(list)
        }

        fun getItemGroupByID(id:Int): ItemGroup? = ItemGroups.find { it.ID == id }

        fun findGroupByIDMain(groupID: Int): ItemGroup {
            return ItemGroups.find { it.ID == groupID } ?: throw Exception("non existing group")
        }

        fun findGroupByIDTrash(groupID: Int): ItemGroup {
            return TrashItemGroups.find { it.ID == groupID } ?: throw Exception("non existing group")
        }

        fun updateRecentItems(item: Item){
            if (RecentItems.indexOf(item) == -1){
                item.Amount = 0
                RecentItems.add(item)
            }
        }

        fun moveGroupToTrash(groupID: Int) {

            val group = findGroupByIDMain(groupID)
            val request = ServiceBuilder.buildService(BackendEndpoint::class.java)


        }

        fun restoreGroup(groupID: Int) {

        }

        fun permaDeleteGroup(groupID: Int) {
            service.deleteList(groupID).enqueue(object: Callback<ResponseBody>{
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    println(t.message)
                }

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    TrashItemGroups = ArrayList(TrashItemGroups.filter { it.ID != groupID })
                }
            })
        }
}

val itemCategories =
    ArrayList(
        mutableListOf(
            ItemCategory(0, "Food", R.drawable.ic_turkey),
            ItemCategory(1, "hygiene", R.drawable.ic_deodorant),
            ItemCategory(2, "beverages", R.drawable.ic_beer)
        ))

val popularItems =
    ArrayList(
        mutableListOf(Item(0, "milk", false, 0, R.drawable.ic_milk, 2),
            Item(1, "bread", false, 0, R.drawable.ic_bread, 0),
            Item(2, "eggs", false, 0, R.drawable.ic_eggs, 0),
            Item(3, "butter", false, 0, R.drawable.ic_butter, 0),
            Item(4, "cheese", false, 0, R.drawable.ic_cheese, 0),
            Item(5, "toilet paper", false, 0, R.drawable.ic_papers, 1),
            Item(6, "chicken", false, 0, R.drawable.ic_turkey, 0),
            Item(7, "potatoes", false, 0, R.drawable.ic_potatoes, 0),
            Item(8, "lettuce", false, 0, R.drawable.ic_lettuce, 0),
            Item(9, "toothpaste", false, 0, R.drawable.ic_toothpaste, 1),
            Item(10, "juice", false, 0, R.drawable.ic_juice, 2),
            Item(11, "shampoo", false, 0, R.drawable.ic_shampoo, 1),
            Item(12, "water", false, 0, R.drawable.ic_hydratation, 2),
            Item(13, "ketchup", false, 0, R.drawable.ic_ketchup, 2),
            Item(14, "rice", false, 0, R.drawable.ic_rice, 0),
            Item(15, "ham", false, 0, R.drawable.ic_ham, 0),
            Item(16, "cucumber", false, 0, R.drawable.ic_cucumber, 0),
            Item(17, "pasta", false, 0, R.drawable.ic_pasta, 0),
            Item(18, "ice cream", false, 0, R.drawable.ic_ice_cream, 0),
            Item(19, "tuna", false, 0, R.drawable.ic_tuna, 0),
            Item(20, "olive oil", false, 0, R.drawable.ic_olive_oil, 0),
            Item(21, "tea", false, 0, R.drawable.ic_olive_oil, 2),
            Item(22, "beer", false, 0, R.drawable.ic_beer, 2),
            Item(23, "conditioner", false, 0, R.drawable.ic_conditioner, 1),
            Item(24, "sausage", false, 0, R.drawable.ic_sausage, 0),
            Item(25, "cookies", false, 0, R.drawable.ic_cookies, 0),
            Item(26, "landry detergent", false, 0, R.drawable.ic_laundry_detergent, 1),
            Item(27, "mustard", false, 0, R.drawable.ic_mustard, 2),
            Item(28, "mayonaisse", false, 0, R.drawable.ic_mayonaisse, 0),
            Item(29, "chocolate", false, 0, R.drawable.ic_chocolate, 0),
            Item(30, "turkey", false, 0, R.drawable.ic_turkey, 0),
            Item(31, "tampons", false, 0, R.drawable.ic_tampons, 1),
            Item(32, "shower gel", false, 0, R.drawable.ic_shower_gel, 1),
            Item(33, "deodorant", false, 0, R.drawable.ic_deodorant, 1),
            Item(34, "chicken nuggets", false, 0, R.drawable.ic_chicken_nuggets, 0),
            Item(35, "baby food", false, 0, R.drawable.ic_baby_food, 0),
            Item(36, "pizza", false, 0, R.drawable.ic_pizza, 0)))