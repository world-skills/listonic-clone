package com.example.listonic

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ViewAnimationUtils
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageButton
import android.widget.Toast
import androidx.cardview.widget.CardView
import com.example.listonic.api.BackendEndpoint
import com.example.listonic.api.ServiceBuilder
import com.example.listonic.datamanager.DataManager
import com.example.listonic.model.ItemGroup
import kotlinx.android.synthetic.main.create_list_dialog_layout.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_list)

        supportActionBar?.hide()
        val ttb = AnimationUtils.loadAnimation(this, R.anim.ttp)

        overridePendingTransition(R.anim.ttp, R.anim.ttp)
        val cardView = findViewById<CardView>(R.id.cardView)

        cardView.startAnimation(ttb)
        val btnClose = findViewById<ImageButton>(R.id.btnClose)
        btnClose.setOnClickListener{
            finish()
        }

        val btnCreate = findViewById<Button>(R.id.btnCreateList)
        btnCreate.setOnClickListener {
            val request=  ServiceBuilder.buildService(BackendEndpoint::class.java)
            var name = txtNewListName.text.toString()
            if (name == ""){
                val defaultName = "New list"
                val n = DataManager.ItemGroups.filter { it.Name.contains(defaultName) }.size

                if (n == 0){
                    name = defaultName
                }else{
                    name = "$defaultName ${n+1}"
                }

            }
            val list = ItemGroup(Name = name, UserID = DataManager.user?.ID!!)
            val call = request.addNewList(list)

            call.enqueue(object: Callback<ItemGroup> {
                override fun onFailure(call: Call<ItemGroup>, t: Throwable) {
                    Toast.makeText(this@AddListActivity, "${t.message}", Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(
                    call: Call<ItemGroup>,
                    response: Response<ItemGroup>
                ) {
                    DataManager.addNewItemGroup(response.body()!!)
                    finish()
                }

            })

        }

    }
}
